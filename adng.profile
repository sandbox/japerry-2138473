<?php
/**
 * @file
 * Enables modules and site configuration for a Acquia Drupal site installation.
 */

/*
 * Define commons minimum execution time required to operate.
 */
define('DRUPAL_MINIMUM_MAX_EXECUTION_TIME', 120);

/**
 * Implements hook_admin_paths_alter().
 */
function adng_admin_paths_alter(&$paths) {
  // Avoid switching between themes when users edit their account.
  $paths['user'] = FALSE;
  $paths['user/*'] = FALSE;
}

/**
 * Implements hook_install_tasks_alter().
 */
function adng_install_tasks_alter(&$tasks, $install_state) {
  global $install_state;

  // Skip profile selection step.
  $tasks['install_select_profile']['display'] = FALSE;

  // Skip language selection install step and default language to English.
  $tasks['install_select_locale']['display'] = FALSE;
  $tasks['install_select_locale']['run'] = INSTALL_TASK_SKIP;
  $install_state['parameters']['locale'] = 'en';
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function adng_form_install_configure_form_alter(&$form, $form_state) {
  // Flush all 'notification' messages, no need to show them to the user.
  adng_clear_messages();
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];

  // Acquia features
  $form['server_settings']['acquia_description'] = array(
    '#type' => 'fieldset',
    '#title' => st('Acquia'),
    '#description' => st('The !an can supplement the functionality of Acquia Drupal by providing enhanced site search (faceted search, content recommendations, content biasing, multi-site search, and others using the Apache Solr service), spam protection (using the Mollom service), and more.  A free 30-day trial is available.', array('!an' => l(t('Acquia Network'), 'http://acquia.com/products-services/acquia-network', array('attributes' => array('target' => '_blank'))))),
    '#weight' => -11,
  );
  $form['server_settings']['enable_acquia_connector'] = array(
    '#type' => 'checkbox',
    '#title' => 'Use Acquia Network Connector',
    '#default_value' => 1,
    '#weight' => -10,
    '#return_value' => 1,
  );
  $form['server_settings']['acquia_connector_modules'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Acquia Network Connector Modules',
    '#options' => array(
      'acquia_agent' => 'Acquia Agent',
      'acquia_search' => 'Acquia Search',
      'acquia_spi' => 'Acquia SPI',
    ),
    '#default_value' => array(
      'acquia_agent',
      'acquia_spi',
    ),
    '#weight' => -9,
    '#states' => array(
      'visible' => array(
        ':input[name="enable_acquia_connector"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['#submit'][] = 'adng_check_acquia_connector';
}


/**
 * Implements hook_update_projects_alter().
 */
function adng_update_projects_alter(&$projects) {
  // Enable update status for the Starter profile.
  $modules = system_rebuild_module_data();
  // The module object is shared in the request, so we need to clone it here.
  $adng = clone $modules['adng'];
  $adng->info['hidden'] = FALSE;
  _update_process_info_list($projects, array('adng' => $adng), 'module', TRUE);
}

/**
 * Implements hook_install_tasks().
 *
 * Allows the user to set a welcome message for anonymous users
 */
function adng_install_tasks() {
  // Suppress any status messages generated during batch install.
  adng_clear_messages();

  //make sure we have more memory than 196M. if not lets try to increase it.
  if (ini_get('memory_limit') != '-1' && ini_get('memory_limit') <= '196M' && ini_get('memory_limit') >= '128M') {
    ini_set('memory_limit', '196M');
  }

  $acquia_connector = variable_get('adng_install_acquia_connector', FALSE);

  return array(
    'adng_acquia_connector_enable' => array(
      'display' => FALSE,
      'type' => '',
      'run' => $acquia_connector ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
    ),
    'adng_revert_features' => array(
      'display' => FALSE,
    ),
    'adng_admin_permissions' => array(
      'display' => FALSE,
    ),
  );
}

/*
 * Revert Features after the installation.
 */
function adng_revert_features() {
  // Revert Features components to ensure that they are in their default states.
  $revert = array(
    'adng_feature' => array('field_instance', 'field_base'),
  );
  features_revert($revert);
}

function adng_admin_permissions() {
  //get the administrator role, we set this in the install file
  $admin_role = user_role_load_by_name('administrator');
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
}

/**
 * Check if the Acquia Connector box was selected.
 */
function adng_check_acquia_connector($form_id, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['enable_acquia_connector']) && $values['enable_acquia_connector'] == 1) {
    $options = array_filter($values['acquia_connector_modules']);
    variable_set('adng_install_acquia_connector', TRUE);
    variable_set('adng_install_acquia_modules', array_keys($options));
  }
}

/**
 * Helper function to generate a machine name similar to the user's full name.
 */
function adng_normalize_name($name) {
  return drupal_strtolower(str_replace(' ','_', $name));
}

/**
 * Enable Acquia Connector module if selected on site configuration step.
 */
function adng_acquia_connector_enable() {
  $modules = variable_get('adng_install_acquia_modules', array());
  if (!empty($modules)) {
    module_enable($modules, TRUE);
    adng_clear_messages();
  }
}

/**
 * Override of install_finished() without the useless text.
 */
function adng_install_finished(&$install_state) {
  // BEGIN copy/paste from install_finished().
  // Remove the bookmarks flag
  $flag = flag_get_flag('bookmarks');
  if($flag) {
    $flag->delete();
    $flag->disable();
    _flag_clear_cache();
  }

  // Flush all caches to ensure that any full bootstraps during the installer
  // do not leave stale cached data, and that any content types or other items
  // registered by the installation profile are registered correctly.
  drupal_flush_all_caches();

  // We make custom code for the footer here because we want people to be able to freely edit it if they wish.
  $footer_body = '<p>'. st('An exceptional Web Experience, powered by <a href="@acquia">Acquia Drupal</a>', array('@acquia' => url('https://www.acquia.com/products-services/acquia-drupal‎'))) . '</p>';

  $footer_block_text = array(
    'body' => st($footer_body),
    'info' => st('Default Footer'),
    'format' => 'full_html',
  );

  if (drupal_write_record('block_custom', $footer_block_text)) {
    $footer_block = array(
      'module' => 'block',
      'delta' => $footer_block_text['bid'],
      'theme' => 'omega',
      'visibility' => 0,
      'region' => 'footer',
      'status' => 1,
      'pages' => 0,
      'weight' => 1,
      'title' => variable_get('site_name', 'Acquia Drupal'),
    );
    drupal_write_record('block', $footer_block);
  }

  // Remember the profile which was used.
  variable_set('install_profile', drupal_get_profile());

  // Installation profiles are always loaded last
  db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('type', 'module')
    ->condition('name', drupal_get_profile())
    ->execute();

  // Cache a fully-built schema.
  drupal_get_schema(NULL, TRUE);

  // Run cron to populate update status tables (if available) so that users
  // will be warned if they've installed an out of date Drupal version.
  // Will also trigger indexing of profile-supplied content or feeds.
  drupal_cron_run();
  // END copy/paste from install_finished().

  if (isset($messages['error'])) {
    $output = '<p>' . (isset($messages['error']) ? st('Review the messages above before visiting <a href="@url">your new site</a>.', array('@url' => url(''))) : st('<a href="@url">Visit your new site</a>.', array('@url' => url('')))) . '</p>';
    return $output;
  }
  else {
    // Since any module can add a drupal_set_message, this can bug the user
    // when we redirect him to the front page. For a better user experience,
    // remove all the message that are only "notifications" message.
    adng_clear_messages();
    // If we don't install drupal using Drush, redirect the user to the front
    // page.
    if (!drupal_is_cli()) {
      drupal_goto('');
    }
  }
}

/**
 * Clear all 'notification' type messages that may have been set.
 */
function adng_clear_messages() {
  drupal_get_messages('status', TRUE);
  drupal_get_messages('completed', TRUE);
  // Migrate adds its messages under the wrong type, see #1659150.
  drupal_get_messages('ok', TRUE);
}