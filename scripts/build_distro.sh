#!/bin/bash
set -e

# this function is no longer needed because we're in one repo now.
pull_git() {
    cd $BUILD_PATH/adng_profile
    if [[ -n $RESET ]]; then
      git reset --hard HEAD
    fi
    git pull origin 7.x-2.x

    cd $BUILD_PATH/repos/modules
    for i in `ls | awk -F/ '{print $1}'`; do
      echo $i
      cd $i
      if [[ -n $RESET ]]; then
        git reset --hard HEAD
      fi
      git pull origin
      cd ..
    done
}

release_notes() {
  rm -rf rn.txt
  pull_git $BUILD_PATH
  OUTPUT="<h2>Release Notes for $RELEASE</h2>"
  cd $BUILD_PATH/adng_profile
  OUTPUT="$OUTPUT <h3>Acquia Drupal:</h3> `drush rn --date $FROM_DATE $TO_DATE`"

  ## old repos. don't use this anymore
  # cd $BUILD_PATH/repos/modules
  # for i in "${modules[@]}"; do
  #  echo $i
  #  cd $i
  #  RN=`drush rn --date $FROM_DATE $TO_DATE`
  #  if [[ -n $RN ]]; then
  #    OUTPUT="$OUTPUT <h3>$i:</h3> $RN"
  #  fi
  #  cd ..
  #done
  #cd $BUILD_PATH/repos/themes/adng_origins
  #RN=`drush rn --date $FROM_DATE $TO_DATE`
  #if [[ -n $RN ]]; then
  #  OUTPUT="$OUTPUT <h3>adng_origins:</h3> $RN"
  #fi

  echo $OUTPUT >> $BUILD_PATH/rn.txt
  echo "Release notes for $RELEASE created at $BUILD_PATH/rn.txt"
}

build_distro() {
    if [[ -d $BUILD_PATH ]]; then
        cd $BUILD_PATH
        #backup the sites directory
        if [[ -d docroot ]]; then
          tar -czvf $BUILD_PATH/sites-.tar.gz

        rm -rf ./docroot
        # do we have the profile?
        if [[ -d $BUILD_PATH/adng_profile ]]; then
          if [[ -d $BUILD_PATH/repos ]]; then
            rm -f /tmp/adng.tar.gz
            drush make --no-cache --no-core --contrib-destination --tar $BUILD_PATH/adng_profile/drupal-org.make /tmp/adng
            drush make --no-cache --prepare-install --drupal-org=core $BUILD_PATH/adng_profile/drupal-org-core.make ./docroot
          else
            mkdir $BUILD_PATH/repos
            mkdir $BUILD_PATH/repos/modules
            mkdir $BUILD_PATH/repos/themes
            build_distro $BUILD_PATH
          fi
          # symlink the profile to our dev copy
          chmod -R 777 $BUILD_PATH/docroot/sites/default
          rm -rf docroot/profiles/adng
          ln -s $BUILD_PATH/adng_profile docroot/profiles/adng
          cd docroot/profiles
          tar -zxvf /tmp/adng.tar.gz
          chmod -R 775 $BUILD_PATH/docroot/profiles/adng
        else
          if [[ -n $USERNAME ]]; then
            git clone --branch 7.x-2.x ${USERNAME}@git.drupal.org:sandbox/japerry/2138473.git adng_profile
          else
            git clone --branch 7.x-2.x http://git.drupal.org/sandbox/japerry/2138473.git adng_profile
          fi
          build_distro $BUILD_PATH
        fi
    else
      mkdir $BUILD_PATH
      build_distro $BUILD_PATH $USERNAME
    fi
  fi
}

# This allows you to test the make file without needing to upload it to drupal.org and run the main make file.
update() {
  if [[ -d $DOCROOT ]]; then
    cd $DOCROOT
    # do we have the profile?
    if [[ -d $DOCROOT/profiles/adng ]]; then
      # do we have an installed adng profile?
        rm -f /tmp/docroot.tar.gz
        rm -f /tmp/adng.tar.gz
        drush make --tar --drupal-org=core profiles/adng/drupal-org-core.make /tmp/docroot
        drush make --tar --drupal-org profiles/adng/drupal-org.make /tmp/adng
        cd ..
        tar -zxvf /tmp/docroot.tar.gz
        cd docroot/profiles/adng/modules/contrib
        # remove the symlinks in the repos before we execute
        find . -type l | awk -F/ '{print $2}' > /tmp/repos.txt
        cd $DOCROOT/profiles
        # exclude repos since we're updating already by linking it to the repos directory.
        UNTAR="tar -zxvf /tmp/adng.tar.gz -X /tmp/repos.txt"
        eval $UNTAR
        echo "Successfully Updated drupal from make files"
        exit 0
    fi
  fi
  echo "Unable to find Build path or drupal root. Please run build first"
  exit 1
}

case $1 in
  pull)
    if [[ -n $2 ]]; then
      BUILD_PATH=$2
      if [[ -n $3 ]]; then
       RESET=1
      fi
    else
      echo "Usage: build_distro.sh pull [build_path]"
      exit 1
    fi
    pull_git $BUILD_PATH $RESET;;
  build)
    if [[ -n $2 ]]; then
      BUILD_PATH=$2
    else
      echo "Usage: build_distro.sh build [build_path]"
      exit 1
    fi
    if [[ -n $3 ]]; then
      USERNAME=$3
    fi
    build_distro $BUILD_PATH $USERNAME;;
  rn)
    if [[ -n $2 ]] && [[ -n $3 ]] && [[ -n $4 ]] && [[ -n $5 ]]; then
      BUILD_PATH=$2
      RELEASE=$3
      FROM_DATE=$4
      TO_DATE=$5
    else
      echo "Usage: build_distro.sh rn [build_path] [release] [from_date] [to_date]"
      exit 1
    fi
    release_notes $BUILD_PATH $RELEASE $FROM_DATE $TO_DATE;;
  update)
    if [[ -n $2 ]]; then
      DOCROOT=$2
    else
      echo "Usage: build_distro.sh update [DOCROOT]"
      exit 1
    fi
    if [[ -n $3 ]]; then
      USERNAME=$3
    fi
    update $DOCROOT;;
esac
